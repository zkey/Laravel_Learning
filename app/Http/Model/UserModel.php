<?php
/**
 * Created by PhpStorm.
 * User: john james
 * Date: 2018/12/25
 * Time: 17:50
 */
namespace App\Http\Model;
use Illuminate\Database\Eloquent\Model;
class UserModel extends Model{
    protected $table="user";
    protected $fillable=["name","id"];
    protected $guarded=["id"];

    //获取所有数据
    public function findAll()
    {
       return  $this->all()->toArray();
    }

    //获取单条数据
    public  function getOne($id){
        $oneRows = $this->find($id);
        return $oneRows==null?array():$oneRows->toArray();
    }
}