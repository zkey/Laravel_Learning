<?php
/**
 * Created by PhpStorm.
 * User: john james
 * Date: 2018/12/25
 * Time: 19:02
 */
namespace  App\Http\Model;
use Emadadly\LaravelUuid\Uuids;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
class MongodbModel extends Model{
    use Uuids;
    public $incrementing = false;
    use SoftDeletes;

    protected $connection = 'mongodb';
    protected $collection = 'xxx表名';     //表名
    protected $primaryKey = 'id';    //设置主键
    protected $fillable = [ 'id','title', 'type','xx'];  //设置字段白名单


    /**
     * 需要被转换成日期的属性。
     *
     * @var array
     */

    protected $dates = ['deleted_at'];
}